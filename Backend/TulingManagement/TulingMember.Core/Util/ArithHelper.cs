﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Core
{
    public class ArithHelper
    {
        public static decimal? Multi(decimal? one, decimal? two) {
            if (one==null|| two == null)
            {
                return null;
            }
            return Math.Round(one.Value * two.Value, 2, MidpointRounding.AwayFromZero);
        }
    }
}
