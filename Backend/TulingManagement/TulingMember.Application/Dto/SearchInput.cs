﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application
{
    public class SearchInput
    {
        public string keyword { get; set; }
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? sdate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? edate { get; set; }
      
    }
}
