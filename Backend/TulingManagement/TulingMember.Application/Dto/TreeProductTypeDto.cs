﻿using JoyAdmin.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application.Dto
{
    public class TreeProductTypeDto : ITreeNode
    {
        public long Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long ParentId { get; set; }

        public int Sort { get; set; }
        public List<TreeProductTypeDto> Children { get; set; } = new List<TreeProductTypeDto>();

        public long GetId()
        {
            return Id;
        }

        public long GetParentId()
        {
            return ParentId;
        }

        public void SetChildren(IList children)
        {
            Children = (List<TreeProductTypeDto>)children;
        }

        
    }
}
