﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Core;

namespace TulingMember.Application 
{
    public class DtoHelper
    {
        public static ProductDto TransProductDto(cts_Product p,cts_ProductType t) {
            var s= p.Adapt<ProductDto>();
            s.TypeName = t.Name;
            return s;
        }

    }
}
